This repository is a private one that is used for SE2811 [Lab 6][lab6] and [7][lab7] and [8][lab8] which is to implement a program with the decorator and composite pattern and command pattern.

[lab6]: https://faculty-web.msoe.edu/yoder/se2811/Lab6
[lab7]: https://faculty-web.msoe.edu/yoder/se2811/Lab7
[lab8]: https://faculty-web.msoe.edu/yoder/se2811/Lab8

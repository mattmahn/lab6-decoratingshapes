package longatoj_mahnkem.ui;

import longatoj_mahnkem.shapes.CompositeShape;
import longatoj_mahnkem.shapes.ShapeManager;

/**
 * A class that wraps the decompose execute and unexecute commands for a shape that this class will govern
 *
 * @author Matthew Mahnke
 */
public class DecomposeCommand implements Command {
	/**
	 * The shape that this command governs
	 */
	private CompositeShape compositeShape;

	/**
	 * Constructs a Decompose Command for the CompositeShape passed in
	 *
	 * @param shapes Shape to govern
	 */
	public DecomposeCommand(CompositeShape shapes) {
		this.compositeShape = shapes;

	}

	/**
	 * Executes the decompose method of the governed shape.
	 */
	@Override
	public void execute() {
		ShapeManager.getInstance().decomposeComposite(compositeShape);
	}

	/**
	 * Adds this newly created CompositeShape to the display
	 */
	@Override
	public void unexecute() {
		ShapeManager.getInstance().addCompositeShape(compositeShape);
	}
}

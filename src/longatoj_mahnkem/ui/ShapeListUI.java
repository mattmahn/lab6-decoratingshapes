package longatoj_mahnkem.ui;

import longatoj_mahnkem.shapes.CompositeShape;
import longatoj_mahnkem.shapes.Shape;
import longatoj_mahnkem.shapes.ShapeManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;

/**
 * @author hornick ShapeListUI - implements the UI that displays a list of shapes in a collection
 */
public class ShapeListUI extends JFrame implements java.util.Observer {

	private JList<Shape> jlContents;
	private String cmdCombine = "Combine";
	private String cmdDecompose = "Decompose";
	private ShapeManager shapeManager;

	/**
	 * Class constructor: creates the UI
	 */
	public ShapeListUI(ShapeManager shapeManager) {
		// This is required so that an infinite number of this window doesn't spawn, crashing Intellij
		this.shapeManager = shapeManager;
		EventHandler eventHandler = new EventHandler();

		//Window
		setTitle("SE2811 Shape List"); // initial title
		setSize(250, 400);
		setLocation(200, 200);
		setResizable(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);

		Container contentPane = getContentPane();

		BorderLayout bl = new BorderLayout(0, 0);
		contentPane.setLayout(bl);

		//Panel that contains the add, cancel, and save buttons
		JPanel jp = new JPanel();
		jp.setLayout(new FlowLayout(FlowLayout.CENTER));
		jp.setBackground(new Color(113, 142, 169));

		//"Add" button
		JButton btnCombine = new JButton(cmdCombine);
		//btnAdd.setBounds(20, 400, 139, 50);
		btnCombine.addActionListener(eventHandler);
		jp.add(btnCombine);

		//"Remove" button
		JButton btnDecompose = new JButton(cmdDecompose);
		//btnAdd.setBounds(20, 400, 139, 50);
		btnDecompose.addActionListener(eventHandler);
		jp.add(btnDecompose);

		contentPane.add(jp, BorderLayout.SOUTH);

		jlContents = new JList<>(new DefaultListModel<>());
		Font font = new Font("Arial", Font.PLAIN, 12);
		jlContents.setFont(font);
		JScrollPane spPLContents = new JScrollPane(jlContents,
		                                           JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		                                           JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);    // CCT lab 6
		spPLContents.setBounds(20, 40, 458, 340);
		contentPane.add(spPLContents, BorderLayout.CENTER);

		//"Contents" label
		JLabel lblContents = new JLabel();
		lblContents.setBounds(20, 20, 458, 20);
		lblContents.setText("Shape contents:");
		contentPane.add(lblContents, BorderLayout.NORTH);

		contentPane.validate();
		contentPane.repaint();

		//observe ShapeShapeManager.getInstance()
		shapeManager.addObserver(this);
	}

	/**
	 * Override of java.util.Observable's update() method. Regenerates the list of shapes contained in the
	 * ShapeManager.getInstance()'s collection
	 */
	@Override
	public void update(Observable o, Object arg) {
		DefaultListModel<Shape> list = (DefaultListModel<Shape>) jlContents.getModel();
		list.clear();
		List<Shape> shapes = (List<Shape>) arg;
		System.out.println("shapes = " + shapes);
		System.out.println("list = " + list);
		shapes.forEach(list::addElement);
	}

	/**
	 * Gathers the selected shapes from the list and calls the appropriate ShapeShapeManager.getInstance() methods to
	 * composite the selected shapes
	 */
	private void handleCombine() {
		CommandInvoker.getInstance().invokeCommand(new CombineCommand(jlContents.getSelectedValuesList()));
	}

	/**
	 * Gathers the selected shapes from the list and calls the appropriate ShapeShapeManager.getInstance() methods to
	 * decompose the selected composites; ignores non-composite selections
	 */
	private void handleDecompose() {
		jlContents.getSelectedValuesList().stream()
		          .filter(s -> s instanceof CompositeShape)
		          .forEach(s -> CommandInvoker.getInstance().invokeCommand(new DecomposeCommand((CompositeShape) s)));
	}

	/**
	 * ShapeHierarchyUI event handler
	 *
	 * @author hornick
	 */
	private class EventHandler implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent event) {
			String cmd = event.getActionCommand();
			if (cmd.equals(cmdCombine)) {
				handleCombine();
			} else if (cmd.equals(cmdDecompose)) {
				handleDecompose();
			}
		}
	}
}


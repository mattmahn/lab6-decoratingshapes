package longatoj_mahnkem.ui;

/**
 * The Command class represents an abstract command that is invoked by some class.
 *
 * @author Matthew Mahnke
 */
public interface Command {
	/**
	 * This method is called when this specific command should run
	 */
	public void execute();

	/**
	 * This method is called when this specific command should be reversed
	 */
	public void unexecute();
}

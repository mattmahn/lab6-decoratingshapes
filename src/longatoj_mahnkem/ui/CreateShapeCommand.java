package longatoj_mahnkem.ui;

import longatoj_mahnkem.shapes.Shape;
import longatoj_mahnkem.shapes.ShapeManager;

import java.awt.*;

/**
 * This class encapsulates a command that creates a new {@link longatoj_mahnkem.shapes.Shape Shape}.
 * <p>
 * When an instance of this class is constructed, the ShapeManager's {@link longatoj_mahnkem.shapes.ShapeManager#createNewShape(java.awt.Point,
 * java.awt.Point) createNewShape} is called
 *
 * @author Matthew Mahnke
 */
public class CreateShapeCommand implements Command {
	/**
	 * The newly created shape
	 */
	private Shape newShape;

	/**
	 * Creates a new shape to be drawn
	 *
	 * @param start coordinate specifying where the new shape starts
	 * @param end   coordinate specifying where the new shape ends
	 */
	public CreateShapeCommand(Point start, Point end) {
		this.newShape = ShapeManager.getInstance().createNewShape(start, end);
	}

	/**
	 * Adds this newly created shape to the display
	 * <p>
	 * NOTE: Does not add any <tt>null</tt> shapes
	 */
	@Override
	public void execute() {
		if (newShape != null) {
			ShapeManager.getInstance().addShape(newShape);
		}
	}

	/**
	 * Removes this shape from the display
	 */
	@Override
	public void unexecute() {
		ShapeManager.getInstance().removeShape(newShape);
	}
}

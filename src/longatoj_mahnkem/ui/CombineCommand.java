package longatoj_mahnkem.ui;

import longatoj_mahnkem.shapes.CompositeShape;
import longatoj_mahnkem.shapes.Shape;
import longatoj_mahnkem.shapes.ShapeManager;

import java.util.List;

/**
 * This class encapsulates a command that creates a new {@link longatoj_mahnkem.shapes.CompositeShape CompositeShape}.
 *
 * @author Matthew Mahnke
 */
public class CombineCommand implements Command {
	/**
	 * The newly created CompositeShape
	 */
	private CompositeShape compositeShape;

	/**
	 * Creates a new CompositeShape to be drawn
	 *
	 * @param shapes the shapes to combine into one CompositeShape
	 */
	public CombineCommand(List<Shape> shapes) {
		this.compositeShape = new CompositeShape(shapes);
	}

	/**
	 * Adds this newly created CompositeShape to the display
	 */
	@Override
	public void execute() {
		ShapeManager.getInstance().addCompositeShape(compositeShape);
	}

	/**
	 * Decomposes this composite into it's constituent shapes
	 */
	@Override
	public void unexecute() {
		ShapeManager.getInstance().decomposeComposite(compositeShape);
	}
}

package longatoj_mahnkem.ui;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * CommandInvoker is designed to be the only class that invokes any {@link longatoj_mahnkem.ui.Command Command's}
 * <tt>{@link longatoj_mahnkem.ui.Command#execute() execute()}</tt> and <tt>{@link
 * longatoj_mahnkem.ui.Command#unexecute() unexecute()}</tt> methods.
 *
 * @author Matthew Mahnke
 */
public class CommandInvoker {
	/**
	 * The singleton instance of this class
	 */
	private static volatile CommandInvoker uniqueInstance;

	/**
	 * The list of commands that have been triggered
	 */
	private Stack<Command> cmdStack;

	/**
	 * Constructs a new CommandInvoker with an empty command stack
	 */
	private CommandInvoker() {
		this.cmdStack = new Stack<>();
	}

	/**
	 * Returns the singleton instance of this class
	 *
	 * @return the singleton instance of this class
	 */
	public static CommandInvoker getInstance() {
		if (uniqueInstance == null) {
			synchronized (CommandInvoker.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new CommandInvoker();
				}
			}
		}

		return uniqueInstance;
	}

	/**
	 * Executes the given command, then synchronously adds the executed command to {@link #cmdStack}.
	 * <p>
	 * Requires a non-null Command
	 *
	 * @param cmd the Command to run
	 */
	public void invokeCommand(Command cmd) {
		if (cmd != null) {
			synchronized (this) {
				cmd.execute();
				cmdStack.push(cmd);
			}
		}
	}

	/**
	 * Reverts the previously executed command's effect.
	 * <p>
	 * This method runs completely synchronized.
	 */
	public synchronized void undo() {
		try {
			cmdStack.pop().unexecute();
		} catch (EmptyStackException e) {
			System.out.println("Cannot undo anymore actions; all actions have been un-done.");
		}
	}
}

package longatoj_mahnkem.ui;

/**
 * DrawingProgramApp is the main class for this application. Doesn't do much except create the UI
 *
 * @author : hornick
 * @version 1.0
 * @created 21-Jan-2014 6:15:19 PM
 */
public class DrawingProgramApp {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DrawingProgramUI();
	}
}

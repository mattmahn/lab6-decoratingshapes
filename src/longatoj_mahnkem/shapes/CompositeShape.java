package longatoj_mahnkem.shapes;

import java.awt.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class that is meant to house all shapes that are created. If another class would like this "Shape" is able to
 * decompose and compose combinations of other composite shapes which will be housed in this class's List of children.
 *
 * @author longatoj
 */
public class CompositeShape implements Shape {
	/**
	 * The list of shapes this composite is composed of
	 */
	protected List<Shape> children;

	/**
	 * Constructs a basic composite shape that has the Shape s as the only child that this composite shape contains.
	 *
	 * @param shapes the child of this new CompositeShape
	 */
	public CompositeShape(List<Shape> shapes) {
		this.children = shapes;
	}

	/**
	 * Adds a shape to this composite
	 *
	 * @param shape the shape to add
	 * @return true (as per {@link java.util.List#add(java.lang.Object)}
	 */
	public boolean add(Shape shape) {
		return children.add(shape);
	}

	/**
	 * Adds a collection of Shapes to this composite
	 *
	 * @param shapes the shapes to add
	 * @return true if {@link #children} changed (as per {@link java.util.List#addAll(java.util.Collection)}
	 */
	public boolean addAll(Collection<Shape> shapes) {
		return children.addAll(shapes);
	}

	/**
	 * Returns the string representation of the children that this class holds
	 */
	@Override
	public String getName() {
		return children.toString();
	}

	/**
	 * Returns the {@link java.awt.Point} nearest to the origin such that none of the {@link #children children's}
	 * points have a smaller <tt>x</tt> or <tt>y</tt> value.
	 *
	 * @return the Point nearest to the origin that bounds the points in children
	 */
	@Override
	public Point getStart() {
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
		List<Point> points = new LinkedList<>();
		children.forEach(child -> points.add(child.getStart()));
		children.forEach(child -> points.add(child.getEnd()));

		for (Point point : points) {
			double x = point.getX(), y = point.getY();
			if (x < minX) {
				minX = (int) x;
			}
			if (y < minY) {
				minY = (int) y;
			}
		}

		return new Point(minX, minY);
	}

	/**
	 * Removes a shape from this composite
	 *
	 * @param shape the shape to remove
	 * @return true if this composite contained the shape
	 */
	public boolean remove(Shape shape) {
		return children.remove(shape);
	}

	/**
	 * Removes all specified shapes from this composite
	 *
	 * @param shapes the shapes to remove
	 * @return true if {@link #children} changed (as per {@link java.util.List#addAll(java.util.Collection)}
	 */
	public boolean removeAll(Collection<Shape> shapes) {
		return children.removeAll(shapes);
	}

	/**
	 * Draws a {@link java.awt.Color#DARK_GRAY} border around all constituent shapes, then draws each shape
	 *
	 * @param g the Graphics context to use for drawing
	 */
	@Override
	public void draw(Graphics g) {
		// draw the enclosing border around the composed Shapes
		Point topLeft = getStart();
		Point botRight = getEnd();
		g.setColor(Color.DARK_GRAY);
		g.drawRect(topLeft.x, topLeft.y, Math.abs(topLeft.x - botRight.x), Math.abs(topLeft.y - botRight.y));
		// Draw the shapes themselves
		for (Shape shape : children) {
			shape.draw(g);
		}
	}

	/**
	 * Returns the average color of the children's color, or the color of the first child
	 *
	 * @return the average color of the children's color, or the color of the first child
	 */
	@Override
	public Color getColor() {
		return new Color((int) children.stream()
		                               .mapToInt(s -> s.getColor().getRGB())
		                               .average()
		                               .orElseGet(() -> children.get(0).getColor().getRGB()));
	}

	/**
	 * Returns the {@link java.awt.Point} farthest from the origin such that none of the {@link #children children's}
	 * points have a smaller <tt>x</tt> or <tt>y</tt> value.
	 *
	 * @return the Point farthest from the origin that bounds the points in children
	 */
	@Override
	public Point getEnd() {
		int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
		List<Point> points = new LinkedList<>();
		children.forEach(child -> points.add(child.getStart()));
		children.forEach(child -> points.add(child.getEnd()));

		for (Point point : points) {
			double x = point.getX(), y = point.getY();
			if (x > maxX) {
				maxX = (int) x;
			}
			if (y > maxY) {
				maxY = (int) y;
			}
		}

		return new Point(maxX, maxY);
	}

	/**
	 * Returns the list of shapes that this composite shape has.
	 *
	 * @return List<Shape> children
	 */
	public List<Shape> getChildren() {
		return children;
	}

	/**
	 * Returns the result of {@link #getName}.
	 * <p>
	 * AVOID USING THIS FOR COMPARISON REASONS
	 *
	 * @return the result of {@link #getName}
	 * @see #getName
	 */
	public String toString() {
		return this.getName();
	}

	/**
	 * Attempts to decompose this composites children.
	 * <p>
	 * Accomplishes this goal by first determining which children are CompositeShape's with children of greater size
	 * than 1. If their children size is equal to 1 then this method will add that shape to a temporary list. It then
	 * finds the children that have more than one shape attached to it and iterates through that list and takes the
	 * individual elements of the children and adds them to the temporary list.
	 *
	 * @return The new list of Shapes that this composite shape is made of.
	 */
	public List<Shape> decompose() {
		List<Shape> composites = children.stream().collect(Collectors.toCollection(LinkedList::new));
		children = composites;
		return children;
	}
}

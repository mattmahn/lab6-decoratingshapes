package longatoj_mahnkem.shapes;

import java.awt.*;

/**
 * @author hornick
 * @version 1.0
 * @created 21-Jan-2014 6:13:49 PM
 */
public interface Shape {

	/**
	 * @param g
	 */
	public abstract void draw(Graphics g);

	public abstract java.awt.Color getColor();

	public abstract Point getEnd();

	public abstract String getName();

	public abstract Point getStart();

	/**
	 * @author hornick
	 * @version 1.0
	 * @created 21-Jan-2014 6:13:49 PM
	 */
	public enum ShapeType {
		/**
		 * the type of basic shapes that can be created
		 */
		Rectangle,
		Ellipse
	}

}

package longatoj_mahnkem.shapes;

import longatoj_mahnkem.decorators.Labeled;
import longatoj_mahnkem.decorators.Outlines;
import longatoj_mahnkem.shapes.Shape.ShapeType;
import longatoj_mahnkem.ui.ShapeListUI;

import java.awt.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * @author hornick, yoder
 * @version 1.0
 * @created 21-Jan-2014 6:13:49 PM
 */
public class ShapeManager extends Observable {
	/**
	 * The unique singleton instance of this class
	 */
	private static volatile ShapeManager uniqueInstance;
	/**
	 * The number automatically assigned to each new Shape created by the ShapeManager; automatically incremented with
	 * each new Shape created.
	 */
	private static int sequenceNumber = 0;

	/**
	 * Fill color for newly created shapes
	 */
	private Color currentFillColor = Color.BLUE;
	/**
	 * If true, newly-created shapes should be labeled
	 */
	private boolean currentLabelFlag = false;
	/**
	 * Outline color for newly-created shapes
	 */
	private Color currentOutlineColor = Color.YELLOW;
	/**
	 * If true, newly-created shapes should have an outline
	 */
	private boolean outlineEnabledFlag = false;
	/**
	 * The currently active shape-creation parameters. Any new shapes created via a call to ShapeManager's addShape()
	 * method will use these parameters. These are the default values, but they may be modified by calling this
	 * ShapeManager's mutator methods.
	 */
	private Shape.ShapeType currentShape = Shape.ShapeType.Rectangle;
	/**
	 * The list of Shapes that this program uses to draw, decompose, and combine.
	 */
	private List<Shape> shapes;

	/**
	 * Default constructor.
	 */
	private ShapeManager() {
		// `this` is required so that an infinite number of ShapeListUIs don't spawn, crashing Intellij
		new ShapeListUI(this);
		this.shapes = new LinkedList<>();
	}

	/**
	 * Returns the singleton instance eof this class
	 *
	 * @return the singleton instance of this class
	 */
	public static ShapeManager getInstance() {
		if (uniqueInstance == null) {
			synchronized (ShapeManager.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new ShapeManager();
				}
			}
		}

		return uniqueInstance;
	}

	/**
	 * Causes the ShapeManager to create a new shape based on the currently selected parameters. Note that the new shape
	 * is not added to {@link #shapes}, therefore it will never be displayed until it is added to <tt>shapes</tt> using
	 * {@link #addShape(longatoj_mahnkem.shapes.Shape)}.
	 *
	 * @param start coordinates specifying where the new shape starts
	 * @param end   coordinates specifying where the new shape ends
	 */
	public Shape createNewShape(Point start, Point end) {
		AbstractShape newShape = null;

		// Create the correct basic shape
		if (getCurrentShape() == ShapeType.Ellipse) {
			newShape = new Ellipse(String.format("E%03d", sequenceNumber),
			                       this.currentFillColor,
			                       start.getLocation(),
			                       end.getLocation());
		} else if (getCurrentShape() == ShapeType.Rectangle) {
			newShape = new Rectangle(String.format("R%03d", sequenceNumber),
			                         this.currentFillColor,
			                         start.getLocation(),
			                         end.getLocation());
		}

		// Add any applicable decorators
		if (isOutlineEnabled()) {
			newShape = new Outlines(getCurrentOutlineColor(), newShape);
		}
		if (isCurrentLabelFlag()) {
			newShape = new Labeled(newShape);
		}

		// Don't consider the new shape a new, valid shape if something bad happened
		if (newShape != null) {
			sequenceNumber++;
		}

		return newShape;
	}

	/**
	 * Adds a new {@link longatoj_mahnkem.shapes.CompositeShape CompositeShape} to manage
	 *
	 * @param compositeShape the new Composite Shape to add
	 */
	public void addCompositeShape(CompositeShape compositeShape) {
		this.shapes.removeAll(compositeShape.getChildren());
		addShape(compositeShape);
	}

	/**
	 * Decomposes the specified CompositeShape and re-adds the children of that CompositeShape to this manager
	 *
	 * @param composite the CompositeShape to decompose
	 */
	public void decomposeComposite(CompositeShape composite) {
		this.shapes.remove(composite);
		addAllShapes(composite.getChildren());
	}

	/**
	 * @return the currentFillColor
	 */
	public Color getCurrentFillColor() {
		return currentFillColor;
	}

	/**
	 * @param currentFillColor the currentFillColor to set
	 */
	public void setCurrentFillColor(Color currentFillColor) {
		this.currentFillColor = currentFillColor;
	}

	/**
	 * @return the currentOutlineColor
	 */
	public Color getCurrentOutlineColor() {
		return currentOutlineColor;
	}

	/**
	 * @param currentOutlineColor the currentOutlineColor to set
	 */
	public void setCurrentOutlineColor(Color currentOutlineColor) {
		this.currentOutlineColor = currentOutlineColor;
	}

	/**
	 * @return the currentShape
	 */
	public ShapeType getCurrentShape() {
		return currentShape;
	}

	/**
	 * @param currentShape the currentShape to set EXTRA CREDIT TODO: Eliminate ShapeType enum and use a FactoryPattern
	 *                     to handle this!
	 */
	public void setCurrentShape(Shape.ShapeType currentShape) {
		this.currentShape = currentShape;
	}

	/**
	 * Iterate over & draw shapes
	 *
	 * @param g target graphics object
	 */
	public void draw(Graphics g) {
		for (Shape shape : this.shapes) {
			shape.draw(g);
		}
	}

	/**
	 * @return the currentLabelFlag
	 */
	public boolean isCurrentLabelFlag() {
		return currentLabelFlag;
	}

	/**
	 * @param currentLabelFlag the currentLabelFlag to set
	 */
	public void setCurrentLabelFlag(boolean currentLabelFlag) {
		this.currentLabelFlag = currentLabelFlag;
	}

	/**
	 * @return true if drawing outlined shapes is enabled.
	 */
	public boolean isOutlineEnabled() {
		return outlineEnabledFlag;
	}

	/**
	 * Adds the specified shape to {@link #shapes}.
	 *
	 * @param shape the Shape to add
	 */
	public void addShape(Shape shape) {
		this.shapes.add(shape);
		this.setChanged();
		this.notifyObservers(shapes);
	}

	/**
	 * Adds all the shapes in <tt>shapeCollection</tt> to this manager.
	 * <p>
	 * Equivalent of calling {@link #addShape(Shape)} for each item in the collection; however, the observers are only
	 * notified once.
	 *
	 * @param shapeCollection the collection of shapes to add
	 */
	public void addAllShapes(Collection<Shape> shapeCollection) {
		this.shapes.addAll(shapeCollection);
		this.setChanged();
		this.notifyObservers(shapes);
	}

	/**
	 * Remove a shape from {@link #shapes}
	 *
	 * @param shape the Shape to be removed, including its descendants
	 */
	public void removeShape(Shape shape) {
		this.shapes.remove(shape);
		this.setChanged();
		this.notifyObservers(shapes);
	}

	/**
	 * @param outlineEnabledFlag if true, enable drawing outlines.
	 */
	public void setOutlineEnabledFlag(boolean outlineEnabledFlag) {
		this.outlineEnabledFlag = outlineEnabledFlag;
	}
}

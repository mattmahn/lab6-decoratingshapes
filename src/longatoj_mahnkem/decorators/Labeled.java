package longatoj_mahnkem.decorators;

import longatoj_mahnkem.shapes.AbstractShape;

import java.awt.*;

/**
 * This class represents a labeled shape
 */
public class Labeled extends Decoration {
	/**
	 * Constructs a a labeled shape with the specified parameters
	 *
	 * @param shape the shape to label
	 * @param name  the name of shape (this is its label)
	 * @param clr   the color of the shape
	 * @param p1    the starting point of this shape
	 * @param p2    the ending point of this shape
	 */
	public Labeled(AbstractShape shape, String name, Color clr, Point p1, Point p2) {
		super(shape, name, clr, p1, p2);
	}

	/**
	 * Constructs a Labeled Shape with other constructor
	 *
	 * @param shape the shape to encapsulate with a label
	 */
	public Labeled(AbstractShape shape) {
		this(shape, shape.getName(), shape.getColor(), shape.getStart(), shape.getEnd());
	}

	/**
	 * Draws the nested, inner shape, then draws the name of the shape at the end point
	 *
	 * @param g the Graphics to draw in
	 */
	@Override
	public void draw(Graphics g) {
		super.draw(g);
		g.drawString(name, end.x, end.y);
	}
}

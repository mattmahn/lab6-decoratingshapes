package longatoj_mahnkem.decorators;

import longatoj_mahnkem.shapes.AbstractShape;
import longatoj_mahnkem.shapes.Ellipse;
import longatoj_mahnkem.shapes.Rectangle;

import java.awt.*;

abstract class Decoration extends AbstractShape {
	/**
	 * The shape that is being decorated
	 */
	protected AbstractShape shape;

	public Decoration(AbstractShape shape, String name, Color clr, Point p1, Point p2) {
		super(name, clr, p1, p2);
		this.shape = shape;
	}

	@Override
	public void draw(Graphics g) {
		shape.draw(g);
	}

	public ShapeType getType() {
		ShapeType shapeType = null;

		if (shape instanceof Ellipse) {
			shapeType = ShapeType.Ellipse;
		} else if (shape instanceof Rectangle) {
			shapeType = ShapeType.Rectangle;
		}

		return shapeType;
	}

}

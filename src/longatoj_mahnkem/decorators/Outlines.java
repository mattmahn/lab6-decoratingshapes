package longatoj_mahnkem.decorators;

import longatoj_mahnkem.shapes.AbstractShape;

import java.awt.*;

public class Outlines extends Decoration {
	/**
	 * The color to draw the outline
	 */
	private Color outLineColor;

	public Outlines(Color Color, AbstractShape shape) {
		super(shape, shape.getName(), shape.getColor(), shape.getStart(), shape.getEnd());
		this.outLineColor = Color;
	}

	@Override
	public void draw(Graphics g) {
		int x = (int) Math.min(start.getX(), end.getX());
		int y = (int) Math.min(start.getY(), end.getY());
		int w = (int) Math.abs(start.getX() - end.getX());
		int h = (int) Math.abs(end.getY() - start.getY());
		g.setColor(outLineColor);

		if (getType() == ShapeType.Ellipse) {
			g.fillOval(x - 2, y - 2, w + 4, h + 4);
		} else if (getType() == ShapeType.Rectangle) {
			g.fillRect(x - 2, y - 2, w + 4, h + 4);
		}

		System.out.println("IN OUTLINES DRAWER");
		super.draw(g);
	}

}
